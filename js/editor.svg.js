class SVGEditor {
    constructor(rootEl, app, content) {
        this.rootEl = rootEl;
        this.rootEl.className = "svgEditor";

        this.app = app;

        this.renderContainer = document.createElement('div');
        this.renderContainer.className = "svgRenderContainer";
        this.rootEl.append(this.renderContainer);

        this.renderer = document.createElement('div');
        this.renderer.id = "svgRender";
        this.renderContainer.append(this.renderer);

        this.textarea = document.createElement('textarea');
        this.textarea.id = "svgRoot";
        this.rootEl.append(this.textarea);

        this.generateButton = document.createElement('button');
        this.generateButton.innerHTML = "<figure>⬇️</figure>PNG";
        this.generateButton.onclick = () => {
            this.generatePNG();
        }

        _("#editorExtensions").append(this.generateButton);

        this.editor = CodeMirror.fromTextArea(this.textarea, {
            mode: "xml",
            htmlMode: true,
            theme: "uxmap",
            scrollPastEnd: true,
            highlightActiveLine: true,
            lineNumbers: true,
            lineWrapping: true
        });

        this.editor.focus();

        this.editor.on('change', (i, op) => {
            this.render();
        });

        this.editor.on('keyup', () => {
            if (this.saveTimer) clearTimeout(this.saveTimer);

            let filename = this.app.selectedFilename;
            let filedata = this.editor.getValue();

            this.saveTimer = setTimeout(() => {
                this.app.storage.writeFile(filename, filedata);
            }, this.app.storage.saveTimer);
        });

        this.editor.setValue(content);
        this.render();
    }
    async generatePNG() {

        const svg = this.renderer.firstChild;
        const width = 512;
        const height = 512;

        var canvas = document.createElement('canvas');
        svg.setAttribute('width', width);
        svg.setAttribute('height', height);
        canvas.width = width;
        canvas.height = height;

        var data = new XMLSerializer().serializeToString(svg);
        var blob = new Blob([data], { type: 'image/svg+xml' });
        var url = window.URL.createObjectURL(blob);
        var img = new Image();

        img.onload = () => {
            canvas.getContext('2d').drawImage(img, 0, 0);
            window.URL.revokeObjectURL(url);
            var uri = canvas.toDataURL('image/png').replace('image/png', 'octet/stream');
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.style = 'display: none';
            a.href = uri
            a.download = this.app.selectedFile.base + ".png";
            a.click();
            window.URL.revokeObjectURL(uri);
            document.body.removeChild(a);
        };
        img.src = url;

        // const svg64 = btoa(svg);
        // const b64Start = 'data:image/svg+xml;base64,';
        // const image64 = b64Start + svg64;

        // let image = new Image();
        // image.src = image64;
        // image.onload = () => {

        //   const canvas = document.createElement('canvas');
        //   canvas.width = 256;
        //   canvas.height = 256;

        //   const context = canvas.getContext('2d');
        //   context.drawImage(image, 0, 0);

        //   this.renderer.append(canvas);

        // }
        // this.renderer.append(image);
        // image.src = svg.toDataURL('image/svg+xml');


        // console.log(image.src);
        // var parser = new DOMParser();
        // var doc = parser.parseFromString(data, "image/svg+xml");



        // const displayImage = await loadImage(png);
        // displayImage.width = dim / 2;
        // displayImage.height = dim / 2;
        // displayImage.title = file.name + '.png';

        // return displayImage;
    }
    render() {
        let svgmarkup = this.editor.getValue();
        this.renderer.innerHTML = svgmarkup;
    };

}
