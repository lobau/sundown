// "use strict";

class LocalForageStorage {
    constructor(app) {
        this.user = "local forage";
        this.saveTimer = 500;
        this.app = app;

        localforage.config({
            name: 'page.noodle.files'
        });

        this.app.setStorageData("Local forage");
    }
    async loadFile(filename) {
        const filecontent = await localforage.getItem(filename);
        return filecontent.content;
    }
    async createFile(filename, template) {
        let file = {
            content: template,
            modified: Date.now()
        }

        await localforage.setItem(filename, file);
    }
    async writeFile(filename, content) {
        let file = {
            content: content,
            modified: Date.now()
        }
        await localforage.setItem(filename, file);
        this.app.selectedFile.modified = new Date();
    }
    async renameFile(oldname, newname) {
        const newfile = await localforage.getItem(newname);

        if (newfile === null) {
            let file = await localforage.getItem(oldname);
            file.name = newname;
            await localforage.setItem(newname, file);
            await localforage.removeItem(oldname);

        } else {
            if (oldname != newname) this.app.toast("A file with this name already exists!", "warning");
        }
    }
    async deleteFile() {
        await localforage.removeItem(this.app.selectedFilename);

        const searchParams = new URLSearchParams(location.search);
        searchParams.set('file', "");
        location.search = searchParams.toString();
    }
    async archiveFile() {

        await this.renameFile(this.app.selectedFilename, "." + this.app.selectedFilename);
        // let archive = await localforage.getItem("__archive");
        // if (archive === null) archive = [];

        // TODO: find a better way to handle archiving
        // Dropbox has a folder
        // Localstorage can have another table? A prefix on filenames?
        // const fileToArchive = await localforage.getItem(this.app.selectedFilename);
        // archive.push(fileToArchive);
        // await localforage.setItem("__archive", archive);
        // await localforage.removeItem(this.app.selectedFilename);

        // const searchParams = new URLSearchParams(location.search);
        // searchParams.set('file', "");
        // location.search = searchParams.toString();
    }
    async downloadFile() {
        const file = await localforage.getItem(this.app.selectedFilename);
        const blob = new Blob([file.content], { type: 'text/plain' });

        const a = document.createElement('a');
        a.setAttribute('download', this.app.selectedFilename);
        a.setAttribute('href', window.URL.createObjectURL(blob));
        a.click();

        setTimeout(() => { URL.revokeObjectURL(blob) }, 1000);

        return false;
    }
    async getFileList() {
        let entries = [];

        await localforage.iterate((value, key, iterationNumber) => {
            let entry = this.app.filenameComponent(key);
            entry.modified = new Date(value.modified);
            if(entry.name.charAt(0) != ".") {
                entries.push(entry);
            }
        })

        return entries;
    }
}
