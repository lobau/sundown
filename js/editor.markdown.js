class MarkdownEditor {
  constructor(rootEl, app, content) {
    this.rootEl = rootEl;
    this.rootEl.className = "markdownEditor";

    this.app = app;

    this.container = document.createElement('div');
    this.container.className = "markdownContainer";
    this.rootEl.append(this.container);

    this.textarea = document.createElement('textarea');
    this.textarea.id = "markdownRoot";
    this.container.append(this.textarea);

    // this.renderContainer = document.createElement('div');
    // this.renderContainer.className = "markdownRenderContainer";
    // this.rootEl.append(this.renderContainer);
    //
    // this.markdownRender = document.createElement('div');
    // this.markdownRender.className = "markdownRender";
    // this.renderContainer.append(this.markdownRender);

    this.editor = CodeMirror.fromTextArea(this.textarea, {
      mode: "markdown",
      theme: "uxmap",
      scrollPastEnd: true,
      highlightActiveLine: false,
      lineNumbers: false,
      lineWrapping: true,

      // htmlMode: true,
      // styleActiveLine: true,
      // autoCloseTags: true,
      // showCursorWhenSelecting: true,
      highlightFormatting: true,
      fencedCodeBlockHighlighting: true,
      fencedCodeBlockDefaultMode: "javascript"
    });

    this.editor.focus();

    // this.editor.on('change', (i, op) => {
    //   this.render();
    // });

    this.editor.on('keyup', () => {
      if (this.saveTimer) clearTimeout(this.saveTimer);

      let filename = this.app.selectedFilename;
      let filedata = this.editor.getValue();

      this.saveTimer = setTimeout(() => {
        this.app.storage.writeFile(filename, filedata);
      }, this.app.storage.saveTimer);
    });

    // customize the renderer
    const renderer = {
      link(href, title, text) {
        return `<a target="_blank" href="${href}" title="${title}">${text}</a>`;
      }
    };
    //
    marked.use({
      renderer
    });
    //
    this.editor.setValue(content);

    this.generateButton = document.createElement('button');
    this.generateButton.innerHTML = "Preview";
    this.generateButton.onclick = () => {
      this.render();
    }

    _("#editorExtensions").append(this.generateButton);

    // this.render();
  }
  render() {
    let markdown = this.editor.getValue();
    let win = window.open('', this.app.selectedFilename);
    win.document.title = this.app.selectedFilename;
    win.document.write(`<!DOCTYPE html><html><head>
  <title>${this.app.selectedFilename}</title>
  <link rel="stylesheet" href="css/app.css" />
  <link rel="stylesheet" href="css/editor.markdown.html.css" />
  </head><body>`);
    win.document.write(marked.parse(markdown));
    win.document.write('</body></html>');
  };



}
